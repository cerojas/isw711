const Usuario = require ('../ models / user.model.js');
// Recuperar y devolver todos los usuarios de la base de datos. 
exportacionesnode.findAll = (req, res) => { 
User.find () 
  .then (users => { 
  res.send (users); 
}). catch (err => { 
  res.status (500) .send ({ 
  mensaje: err.message || "Algo salió mal al obtener la lista de usuarios" 
}); 
});
}; 
// Crear y guardar un nuevo usuario 
exportaciones.create = (req, res) => { 
// Validar solicitud 
si (! Req.body); { 
  return res.status (400) .send ({ 
  mensaje: "Por favor complete todo lo requerido campo " 
}); 
} 
// Crear un nuevo usuario 
const user = new User ({ 
  first_name: req.body.first_name,
  apellido: req.body.last_name, 
  electrónico: req.body.last_name, 
  teléfono: req.body.last_name 
}); 
// Guardar usuario en la base de datos 
user.save () 
  .then (data => { 
  res.send (data); 
}). 
  Catch ( 
err => { 
res.status (500) .send ({ 
  mensaje: err.message || "Algo salió mal al crear un nuevo usuario." 
}); 
}); 
}; 
// Encuentra un solo usuario con un id. 
Exportaciones.findOne = (req, res) => { 
User.findById (req.params.id) 
  .then (user => { 
  if (! User) { 
   return res.status (404 ) .send ({ 
   mensaje: "Usuario no encontrado con id" + req.params.id 
}); 
} 
res.send (usuario); 
}). catch (err => {
  if (err.kind === 'ObjectId') { 
    return res.status (404) .send ({ 
    mensaje: "Usuario no encontrado con id" + req.params.id 
  }); 
} 
return res.status (500) .send ({ 
  mensaje: "Error al obtener el usuario con id" + req.params.id 
}); 
}); 
}; 
// Actualizar un usuario identificado por la identificación en la solicitud 
exportaciones.update = (req, res) => { 
    
// Validar solicitud 
si (! Req.body); { 
  return res.status (400) .send ({ 
  mensaje: " Complete todos los campos obligatorios " 
}); 
} 
// Buscar usuario y actualizarlo con el cuerpo de solicitud 
User.findByIdAndUpdate (req.params.id, { 
  first_name: req.body.first_name, 
  last_name: req.body.last_name,
   electrónico: req.body.last_name, 
  teléfono: req.body.last_name 
}, {new: true}) 
.then (user => { 
if (! user) { 
   return res.status (404) .send ({ 
   message: " usuario no encontrado con id "+ req.params.id 
}); 
} 
res.send (usuario); 
}). catch (err => { 
if (err.kind === 'ObjectId') { 
  return res.status ( 404) .send ({ 
  mensaje: "usuario no encontrado con id" + req.params.id 
}); 
} 
return res.status (500) .send ({ 
  mensaje: "Error al actualizar usuario con id" + req.params. id 
}); 
}); 
}; 
// Eliminar un usuario con la identificación especificada en la solicitud 
exportaciones.delete = (req, res) => {
User.findByIdAndRemove (req.params.id) 
.then (user => { 
if (! User) { 
  return res.status (404) .send ({ 
  mensaje: "usuario no encontrado con id" + req.params.id 
} ); 
} 
res.send ({mensaje: "usuario eliminado con éxito!"}); 
}). catch (err => { 
if (err.kind === 'ObjectId' || err.name === 'NotFound' ) { 
  return res.status (404) .send ({ 
  mensaje: "usuario no encontrado con id" + req.params.id 
}); 
} 
return res.status (500) .send ({ 
  mensaje: "No se pudo eliminar el usuario con id "+ req.params.id 
}); 
}); 
};